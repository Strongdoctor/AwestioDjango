from django.shortcuts import render
from .forms import ContactForm
from .models import ContactMessage, Project
import requests

def homepage(request):
    projects = Project.objects.all()
    return render(request, 'homepage/home.html', {'pagetitle': 'Home', 'projects': projects})

def contact(request):
    if request.method == 'GET':
        form = ContactForm()
    else:
        form = ContactForm(request.POST)
        if form.is_valid():
            contact_email = form.cleaned_data['contact_email']
            message = form.cleaned_data['message']
            msgobj = ContactMessage.objects.create_msg(contact_email=contact_email, message=message)

            mg_key = 'key-f08b15e12441d45a9da1f35a2db1a015'
            mg_domain = 'mg.awest.io'
            mg_recipient = 'strongdoctor@live.com'

            request_url = 'https://api.mailgun.net/v2/{0}/messages'.format(mg_domain)
            requests.post(request_url, auth=('api', mg_key), data={
                'from': 'admin@' + mg_domain,
                'to': mg_recipient,
                'subject': 'Awest.io contact message',
                'text': 'From: ' + str(contact_email) + 'Message: ' + str(message)
            })

            return render(request, 'homepage/contact.html', {'pagetitle': 'Contact', 'form': form, 'contactsuccess': True})

    return render(request, 'homepage/contact.html', {'pagetitle': 'Contact', 'form': form, 'contactsuccess': False})
