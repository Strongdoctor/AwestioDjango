from django.db import models
import django.utils.timezone

class Project(models.Model):
    title = models.CharField(max_length=80)
    description = models.TextField(max_length=254)
    location = models.URLField()
    image = models.ImageField(upload_to='project_images/')
    tags = models.TextField(blank=True)
    uploaded_at = models.DateTimeField(auto_now_add=True)

    def tags_as_list(self):
        return self.tags.strip().split(',')

class ContactMessageManager(models.Manager):
    def create_msg(self, contact_email, message):
        contactmsg = self.create(contact_email=contact_email, message=message)
        return contactmsg

class ContactMessage(models.Model):
    contact_email = models.CharField(max_length=120)
    message = models.TextField(max_length=1000)
    date = models.DateTimeField(default=django.utils.timezone.now)
    objects = ContactMessageManager()
