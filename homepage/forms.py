from django import forms
from captcha.fields import ReCaptchaField

class ContactForm(forms.Form):
	contact_email = forms.EmailField(required=True, label="Your e-mail")
	message = forms.CharField(
		required=True,
		widget=forms.Textarea,
		label="Your message"
	)
	captcha = ReCaptchaField(attrs={
            'theme' : 'dark',
        })
