from django.contrib import admin
from .models import Project, ContactMessage

class ContactMessageAdmin(admin.ModelAdmin):
	list_display = ('contact_email','message', 'date')

admin.site.register(ContactMessage, ContactMessageAdmin)

class ProjectAdmin(admin.ModelAdmin):
	list_display = ('title','uploaded_at')

admin.site.register(Project, ProjectAdmin)
